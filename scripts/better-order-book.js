let bob__observer;

function bob__init() {
    $(".split__container .split__main .footer__wrapper").prepend("<button class='ui-button ui-button--size-XS ui-button--clear' id='runImprovedOrderBook'><i class='fa fa-usd'></i></button>");
    $("#runImprovedOrderBook").click(function() {
        bob__start();
    });
}

function bob__start() {
    console.log('[BBL2] Bitfinex Better Order Book started');

    $("#book-asks > div, #book-bids > div").not(".book__header, .book__bars").find("> div").each(function() {
        bob__watchBook($(this));
    });

    bob__observer = new MutationObserver(bob__onMutation);
    bob__observer.observe($("#book-asks > div").not(".book__header, .book__bars").first()[0], { childList: true });
    bob__observer.observe($("#book-bids > div").not(".book__header, .book__bars").first()[0], { childList: true });

    $("#runImprovedOrderBook").addClass("ui-button--disabled").off('click');
}

function bob__setUSDAmountTooltip(parentId) {
    const $parentNode = $("#" + parentId).first();

    try {
        const $amountNode = $parentNode.find("div:last-child").first();
        $amountNode.addClass("bbl2-tooltip");
        const $priceNode = $parentNode.find("div").not(".bbl2-tooltip").last();

        const amount = parseFloat($amountNode.text().replace(/,/g, ""));
        const price = parseFloat($priceNode.text().replace(/,/g, ""));

        const usdAmount = Math.round(amount * price);

        if ($amountNode.find(".tooltip-text").length === 0)
            $amountNode.append("<span class='tooltip-text'>$"+ formatNumber(usdAmount) +"</span>");
        else
            $amountNode.find(".tooltip-text").first().text("$"+ formatNumber(usdAmount));
    }
    catch(error) {
        console.error(error);
    }
}

function bob__watchBook($book) {
    const uuid = guid();
    $book.attr("id", uuid);
    $book.hover(function() {
        bob__setUSDAmountTooltip(uuid);
    });
}

function bob__onMutation(mutations) {
    mutations.forEach(function( mutation ) {
        const newNodes = mutation.addedNodes;
        const oldNodes = mutation.removedNodes;

        if (newNodes !== null) {
            const $newNodes = $(newNodes);
            $newNodes.each(function() {
                bob__watchBook($(this));
            });
        }

        if (oldNodes !== null) {
            const $oldNodes = $(oldNodes);
            $oldNodes.each(function() {
                $(this).off('hover');
            });
        }
    });
}