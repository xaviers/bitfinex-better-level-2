<h2>
  <img src="https://raw.githubusercontent.com/xsaliniere/bitfinex-better-level-2/master/icons/bbl2-48.png">
  Bitfinex Better Level 2 (version 0.1)
</h2>

Improve Bitfinex level 2 by adding USD amounts on hover and hide other page elements when resizing window.

<p align="center">
  <img src="https://raw.githubusercontent.com/xsaliniere/bitfinex-better-level-2/master/demo.png">
<p>

### Features

* Hide all elements except level 2 when page width is less or equal to 1000px
* Displays the amount in USD when the trade amount is hovered

### Notes

* Support only pages with url like: https://www.bitfinex.com/t/BTC:USD
* Page refresh and navigation are not supported so you have to go directly to the trading page by url or navigate to the page then refresh to enable add-on.
* Bitfinex websocket reconnection and order book/trade list collapsing disable the hover feature so you'll have to refresh the page to re-enable it
* Order book need to be configured to show columns in this specific order: **Count - Total - Price - Amount / Amount - Price - Total - Count**

### How to use

Navigate to your trading page. If you are using bitfinex tickers navigation you will have to refresh the page to enable the add-on.

Once order book and trades list are available, click on the buttons with the dollar icon to enable the hover feature.

## Installing

### Supported browsers
* Firefox

### Download
* Download and install from the [Firefox Add-ons Store](https://addons.mozilla.org/en-US/firefox/addon/bitfinex-better-level-2/)

## License

This project is licensed under the GNU GPL v3 License - see the [LICENSE.md](LICENSE.md) file for details
